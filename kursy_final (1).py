#!/usr/bin/env python
# coding: utf-8
# Zadanie domowe Kursy
# Created by Arkadiusz Dempc


# In[169]:


import pandas as pd


# In[170]:


path = r"C:\Users\arekd\OneDrive\Pulpit\SGH\Python\Kursy\kurs20{}.xlsx"


# In[171]:


df = pd.read_excel(path.format(11))
df.drop_duplicates()
df.dropna(0, how='all')
df.iloc[:,0] = pd.to_datetime(df.iloc[:,0], errors='coerce')
df = df[df['Unnamed: 0'].notna()]
df = df.drop(labels=['Unnamed: 38','Nr tabeli','Unnamed: 40'], axis=1)
df = df.rename({'Unnamed: 0': 'data'}, axis=1)


# In[172]:


df_12 = pd.read_excel(path.format(12))
df_12.drop_duplicates()
df_12.dropna(0, how='all')
df_12.iloc[:,0] = pd.to_datetime(df_12.iloc[:,0], errors='coerce')
df_12 = df_12[df_12['Data'].notna()]
df_12 = df_12.drop(labels=['Nr tabeli','Pełny numer tabeli'], axis=1)
df_12 = df_12.rename({'Data': 'data'}, axis=1)


# In[173]:


df_13 = pd.read_excel(path.format(13))
df_13.drop_duplicates()
df_13.dropna(0, how='all')
df_13.iloc[:,0] = pd.to_datetime(df_13.iloc[:,0], errors='coerce')
df_13 = df_13[df_13['data'].notna()]
df_13 = df_13.drop(labels=['nr tabeli','pełny numer tabeli'], axis=1)


# In[174]:


df_14 = pd.read_excel(path.format(14))
df_14.drop_duplicates()
df_14.dropna(0, how='all')
df_14.iloc[:,0] = pd.to_datetime(df_14.iloc[:,0], errors='coerce')
df_14 = df_14[df_14['data'].notna()]
df_14 = df_14.drop(labels=['nr tabeli','pełny numer tabeli'], axis=1)


# In[175]:


df_15 = pd.read_excel(path.format(15))
df_15.drop_duplicates()
df_15.dropna(0, how='all')
df_15.iloc[:,0] = pd.to_datetime(df_15.iloc[:,0], errors='coerce')
df_15 = df_15[df_15['data'].notna()]
df_15 = df_15.drop(labels=['nr tabeli','pełny numer tabeli'], axis=1)


# In[176]:


df_16 = pd.read_excel(path.format(16))
df_16.drop_duplicates()
df_16.dropna(0, how='all')
df_16.iloc[:,0] = pd.to_datetime(df_16.iloc[:,0], errors='coerce')
df_16 = df_16[df_16['data'].notna()]
df_16 = df_16.drop(labels=['nr tabeli','pełny numer tabeli'], axis=1)


# In[177]:


df_17 = pd.read_excel(path.format(17))
df_17.drop_duplicates()
df_17.dropna(0, how='all')
df_17.iloc[:,0] = pd.to_datetime(df_17.iloc[:,0], errors='coerce')
df_17 = df_17[df_17['data'].notna()]
df_17 = df_17.drop(labels=['nr tabeli','pełny numer tabeli'], axis=1)


# In[178]:


df_18 = pd.read_excel(path.format(18))
df_18.drop_duplicates()
df_18.dropna(0, how='all')
df_18.iloc[:,0] = pd.to_datetime(df_18.iloc[:,0], errors='coerce')
df_18 = df_18[df_18['data'].notna()]
df_18 = df_18.drop(labels=['nr tabeli','pełny numer tabeli'], axis=1)


# In[179]:


df_19 = pd.read_excel(path.format(19))
df_19.drop_duplicates()
df_19.dropna(0, how='all')
df_19.iloc[:,0] = pd.to_datetime(df_19.iloc[:,0], errors='coerce')
df_19 = df_19[df_19['data'].notna()]
df_19 = df_19.drop(labels=['nr tabeli','pełny numer tabeli'], axis=1)


# In[180]:


df_20 = pd.read_excel(path.format(20))
df_20.drop_duplicates()
df_20.dropna(0, how='all')
df_20.iloc[:,0] = pd.to_datetime(df_20.iloc[:,0], errors='coerce')
df_20 = df_20[df_20['data'].notna()]
df_20 = df_20.drop(labels=['nr tabeli','pełny numer tabeli'], axis=1)


# In[181]:


# for year in range(14,15):
#     df = pd.read_excel(path.format(year))
#     df.drop_duplicates()
#     df.dropna(0, how='all')
#     df.iloc[:,0] = pd.to_datetime(df.iloc[:,0], errors='coerce')
#     df = df[df['data'].notna()]
#     df = df_12.drop(labels=['nr tabeli','pełny numer tabeli'], axis=1)
#     final = pd.concat([final, df])
# final


# In[182]:


final = pd.concat([df, df_12, df_13, df_14, df_15, df_16, df_17, df_18, df_19, df_20])
final.set_index('data', inplace=True, drop=True)


# In[183]:


final.columns


# In[184]:


def eur_usd(df):
    return df['1 EUR'].mean(skipna=True)/df['1 USD'].mean(skipna=True)
eur_usd(final)


# In[185]:


final['EURUSD'] = final['1 EUR']/final['1 USD']
final


# In[186]:


def fx_mean(df):
    return df.mean(axis = 0, skipna=True)
# fx_mean(final)
math = pd.DataFrame({'CURR':fx_mean(final).index, 'MEAN':fx_mean(final).values})
math


# In[187]:


def fx_mean_by_curr_name(df, name):
    return df[name].mean(skipna=True)
fx_mean_by_curr_name(final, '1 GBP')


# In[188]:


def fx_max_pln(df):
    return 1/df.min(axis = 0, skipna=True)
fx_max_pln(final)
math['MAXPLN'] = fx_max_pln(final).values
math


# In[189]:


def fx_max_min_pln(df):
    return (1/df.min(axis = 0, skipna=True))-(1/df.max(axis = 0, skipna=True))
fx_max_min_pln(final)
math['MAXMIN'] = fx_max_min_pln(final).values
math


# In[190]:


def fx_variation_pln(df):
    return ((1/df.min(axis = 0, skipna=True))-(1/df.max(axis = 0, skipna=True)))/(1/df.min(axis = 0, skipna=True))
fx_max_min_pln(final)
type(fx_max_min_pln(final))

math['VARIATION'] = math['MAXMIN']/math['MAXPLN']
math


# In[191]:


def min_variation_pln(df):
    return df['VARIATION'].min()
min_variation_pln(math)


# In[192]:


def max_variation_pln(df):
    return df['VARIATION'].max()
max_variation_pln(math)
