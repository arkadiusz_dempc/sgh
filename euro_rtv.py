# -*- coding: utf-8 -*-
"""
Created on Sun Jan  3 14:16:27 2021

@author: Arkadiusz Dempc
"""


from pyquery import PyQuery 
import json
import requests
from bs4 import BeautifulSoup
import pandas as pd

def remove_duplicates(x):
    """Returns list without duplicates."""
    return list(dict.fromkeys(x))

def get_euro_product_list(page_count):
    """Returns list with euro.com.pl direct links for microwaves."""
    product_list = []
    euro_rtv = "https://www.euro.com.pl/kuchenki-mikrofalowe,strona-{}.bhtml"
    for i in range(1,page_count+1):
        euro_rtv_page = requests.get(euro_rtv.format(i)).content
        pq = PyQuery(euro_rtv_page, parser='html')
        pq_collection = pq('h2.product-name').find('a')
        page = [pq_collection.eq(i).attr('href') for i in range(len(pq_collection))]
        no_dup_page = remove_duplicates(page)
        full_url_list = []
        for i in no_dup_page:
            updated_url = "https://www.euro.com.pl" + i
            full_url_list.append(updated_url)
        product_list.extend(full_url_list)
    return product_list

def get_euro_df(html_list):
    """Downloads data from euro.com.pl direct links and returns pandas DataFrame with microwaves specification."""
    final_df = pd.DataFrame()
    for html in html_list:
        r = requests.get(html).content
        
        pq = PyQuery(r)
        product_name = pq('div.product-header').find('h1').text()
        
        soup = BeautifulSoup(r, features="lxml")
        data = json.loads(soup.find('script', type='application/ld+json').text)
        product_price = data['offers']['price']
        
        dfs = pd.read_html(html, attrs={'class': 'description-tech-details js-tech-details'})
        df = dfs[0]
        df = df.dropna()
        df.columns = ['desc', product_name]
        df_price = pd.DataFrame([['Cena', product_price]], columns=df.columns)
        df = df.append(df_price)
        df = df.set_index('desc', drop = True)
        df = df.T
        final_df = final_df.append(df)
    return final_df

def get_allegro_product_list(page_count):
    """Returns list with allegro.pl direct links for microwaves."""
    product_list = []
    allegro = "https://allegro.pl/kategoria/agd-wolnostojace-kuchenki-mikrofalowe-67417?&p={}"
    for i in range(1,page_count+1):
        if i%10 == 0:
            print("Uploading allegro data progress: " + str(i) + "%")
        allegro_page = requests.get(allegro.format(i)).content
        soup = BeautifulSoup(allegro_page, features="lxml")    
        html_container = soup.find_all('h2', class_='mgn2_14 m9qz_yp mqu1_16 mp4t_0 m3h2_0 mryx_0 munh_0')
        for item in html_container:
            page = item.find('a').get('href')
            if 'https://allegrolokalnie' in page:
                continue
            product_list.append(page)
    return remove_duplicates(product_list)


def get_allegro_df(html_list):
    final_df = pd.DataFrame()
    for html in html_list:
        print(html)
        r = requests.get(html).content
        soup = BeautifulSoup(r, features="lxml")
        product_name = soup.find('h1', class_='_9a071_1Ux3M _9a071_3nB-- _9a071_1R3g4 _9a071_1S3No').text
        pln = soup.find('div', class_='_1svub _lf05o _9a071_2MEB_').find('span').text.replace(',','.').strip()
        gr = soup.find('div', class_='_1svub _lf05o _9a071_2MEB_').find('span', class_ = '_qnmdr').text[0:2].strip()
        product_price = pln + gr
        df = pd.DataFrame([[product_name, product_price, html]], columns = ['Allegro Name','Allegro Price','Allegro link'])
        final_df = final_df.append(df)
    return final_df
        
# get_allegro_df(get_allegro_product_list(1)).to_csv('C:/Users/arekd/OneDrive/Pulpit/SGH/Python/Projekt/allegro.csv')

allegro_df = get_allegro_df(get_allegro_product_list(1))
euro_df = get_euro_df(get_euro_product_list(4)).head()

print(allegro_df.query('index.str.contains("Amica AMGF20M1GS")', engine='python'))

