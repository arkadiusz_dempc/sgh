# -*- coding: utf-8 -*-
"""
Created on Sat Jan  9 17:49:10 2021

@author: arekd
"""

import random
from datetime import datetime
import pandas as pd

class PunktOdbioru():
    def __init__(self):
        self.paczki = {}
    
    def odbior(self, paczka):
        paczka.data_przyjecia = datetime.now()
        paczka.status = "PRZYJECIE"
        self.paczki[paczka.kod] = paczka
        
    def drukuj_historie(self):
        historia = pd.DataFrame()
        for paczka in self.paczki.values():
            paczka_historia = pd.DataFrame([[paczka.status,paczka.data_rejestracji,paczka.data_przyjecia,paczka.data_odbioru]], 
                                        columns = ["Status", "Data rejestracji", "Data przyjęcia", "Data odbioru"], index = [paczka.kod])
            historia = pd.concat([historia[~historia.index.isin(paczka_historia.index)], paczka_historia])
        print(historia)

class Paczka():
    def generuj_kod(self, paczki):
        kod = random.randint(111111,999999)
        count = 0
        while count < 10:
            count += 1
            if kod in paczki:
                kod = self.generuj_kod(self, paczki)
            else:
                break
        return kod

    def __init__(self, punkt_odbioru, do_zaplaty = 0):
        self.kod = self.generuj_kod(punkt_odbioru.paczki)
        self.pin = random.randint(1111,9999)
        self.status = "REJESTRACJA"
        self.data_rejestracji = datetime.now()
        self.data_przyjecia = None
        self.data_odbioru = None
        self.do_zaplaty = do_zaplaty
        punkt_odbioru.paczki[self.kod] = self

class Odbiorca():      
    def odbierz_paczke(self, punkt_odbioru, kod, pin, do_zaplaty = 0):
        try:
            paczka = punkt_odbioru.paczki[kod]
            if paczka.pin != pin:
                raise KeyError
            if paczka.data_przyjecia == None:
                print("Paczka nie dotarła jeszcze do punktu odbioru. Prosimy o cierpliwoć.")
            else:
                if paczka.data_odbioru != None:
                    print("Paczka została odebrana dnia:" + paczka.data_odbioru)
                else:
                    saldo = paczka.do_zaplaty - do_zaplaty
                    if saldo > 0:
                        print("Przedmiot nie został opłacony")
                    else:
                        punkt_odbioru.paczki[paczka.kod].status = "ODEBRANO"
                        punkt_odbioru.paczki[paczka.kod].do_zaplaty = saldo
                        punkt_odbioru.paczki[paczka.kod].data_odbioru = datetime.now()
                        print(" dziękujemy za skorzystanie z naszych usług.")
        except:
            print("Nieprawidłowy kod lub pin. Spróbuj ponownie.")

#testy
"""
pkt = PunktOdbioru()
paczka = Paczka(pkt)
p2 = Paczka(punkt1)
punkt1.odbior(p1)
o1 = Odbiorca("Krzysztof", "Krawczyk")
o1.odbierz_paczke(punkt1, p1.kod, p1.pin, 90)
punkt1.drukuj_historie()
p3 = Paczka(punkt1, 122)
punkt1.drukuj_historie()
punkt1.odbior(p3)
punkt1.drukuj_historie()
# """
